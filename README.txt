Description
-----------
Allow copying CCK fields from an existing node into another node on creation.

Installation 
------------
1. Download and install the CCK module.
2. Copy this module's directory to your modules directory and activate the module.
3. Activate it for some content-types at 'admin/content/fieldthief'.


Example
--------
1.	Create a content type named Item.
2.	Add a CCK field called Price (decimal - text field). The machine name of the field is <i>field_price</i>
3.	Create a content type named Order.
4.	Add a cck field called Price from Item (decimal - text field). The machine name of the field is <i>field_price_from_item</i>
5.	Create a node for the Item. Enter the title and a price, example � 20.
6.	Mark the node number. Lets say for the example this node id (nid) is 1.
7.	Go to admin> content management> Field copying rules.
8.	'Add a copy Rule'
9.	To type = order (note: this is the machine readable type, not the content name)
10.	Source arg = 3 � I'll explain this in a few steps
11.	Fields: field_price,field_price_from_item
12.	'Update copy rules'
13.	In the URL write /node/add/order/1

Now the fun begins:
The module is checking if in the URL argument number 3 is a nid. If so it looks in that nid for the field_price and will copy it to the new created Order node.

Note: you can also copy the nid by specifying in the fields for example: 
nid,field_nid_from_other_content


